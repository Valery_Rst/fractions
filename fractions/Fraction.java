package net.fraction;

public class Fraction {
    private int denominator;
    private int numerator;

    public Fraction(int numerator, int denominator) {
        this.numerator = numerator;
        this.denominator = denominator;
    }

    public Fraction() {
        this(1, 1);
    }

    public static Fraction add(Fraction fraction1, Fraction fraction2) {
        if (fraction1.denominator == fraction2.denominator) {
            fraction1.numerator = fraction1.numerator + fraction2.numerator;

        } else if (fraction1.denominator != fraction2.denominator) {
            fraction1.numerator = (fraction1.numerator * fraction2.denominator) + (fraction2.numerator * fraction1.denominator);
            fraction1.denominator = fraction1.denominator * fraction2.denominator;
        }
        reductionOfFraction(fraction1);
        fractionConclusion(fraction1);
        return fraction1;
    }

    public static Fraction subtraction(Fraction fraction1, Fraction fraction2) {
        if (fraction1.denominator == fraction2.denominator) {
            fraction1.numerator = fraction1.numerator - fraction2.numerator;

        } else {
            fraction1.numerator = (fraction1.numerator * fraction2.denominator) - (fraction2.numerator * fraction1.denominator);
            fraction1.denominator = fraction1.denominator * fraction2.denominator;
        }
        reductionOfFraction(fraction1);
        fractionConclusion(fraction1);
        return fraction1;
    }

    public static Fraction multiplication(Fraction fraction1 , Fraction fraction2){

        fraction1.numerator = fraction1.numerator * fraction2.numerator;
        fraction1.denominator = fraction1.denominator * fraction2.denominator;

        reductionOfFraction(fraction1);
        fractionConclusion(fraction1);
        return fraction1;
    }

    public static Fraction division(Fraction fraction1, Fraction fraction2){

        fraction1.numerator = fraction1.numerator * fraction1.denominator;
        fraction1.denominator = fraction1.denominator * fraction2.numerator;

        reductionOfFraction(fraction1);
        fractionConclusion(fraction1);
        return fraction1;
    }

    public static  void reductionOfFraction(Fraction fraction1){
        int min = Math.abs(fraction1.numerator);
        int max = fraction1.denominator;

        for(int i = min; i > 1; i--){
            if((min % i == 0) &&(max % i == 0)){
                fraction1.numerator = fraction1.numerator / i;
                fraction1.denominator = fraction1.denominator / i;
            }
        }
    }

    public static void fractionConclusion(Fraction fraction1) {
        System.out.printf("Результат выражения равен %d/%d %n", fraction1.numerator, fraction1.denominator);
    }
}