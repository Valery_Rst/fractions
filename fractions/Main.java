package net.fraction;

import java.util.Scanner;

public class Main {
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        Fraction tempFraction;
        System.out.println("Напишите ваше выражение так, чтобы знак операции окружали пробелы. Пример: 4/2 + 6/3 ");
        String expression = scanner.nextLine();

        String[] symbols = expression.split(" ");
        String[] op1 = symbols[0].split("/");
        String[] op2 = symbols[2].split("/");

        Fraction fraction1 = new Fraction(Integer.valueOf(op1[0]),Integer.valueOf(op1[1]));
        Fraction fraction2 = new Fraction(Integer.valueOf(op2[0]),Integer.valueOf(op2[1]));

        switch (symbols[1]){
            case "+":
                tempFraction = Fraction.add(fraction1,fraction2);
                fraction1 = tempFraction;
                break;
            case "-":
                tempFraction = Fraction.subtraction(fraction1,fraction2);
                fraction1 = tempFraction;
                break;
            case "*":
                tempFraction = Fraction.multiplication(fraction1,fraction2);
                fraction1 = tempFraction;
                break;
            case "/":
                tempFraction = Fraction.division(fraction1,fraction2);
                fraction1 = tempFraction;
                break;
            default:
                System.out.println("Ошибка. Вы некорректно ввели знак операции. ");
                break;
        }
    }
}
